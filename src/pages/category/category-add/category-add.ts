import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Category } from './../../../shared/interfaces/category.interface';
import { CategoryService } from './../../../shared/services/category.service';

@IonicPage()
@Component({
  selector: 'page-category-add',
  templateUrl: 'category-add.html',
})
export class CategoryAddPage {
  formGroup = new FormGroup({
    name: new FormControl('', [Validators.required])
  });

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private categoryService: CategoryService,
    private viewCtrl: ViewController
  ) { }

  ionViewDidLoad() {
  }

  submit() {
    if (this.formGroup.valid) {
      let category: Category = this.categoryService.registerNewCategory(this.formGroup.value);
      this.closeModal(category);
    }
  }

  closeModal(category: Category): void {
    this.viewCtrl.dismiss(category);
  }

}

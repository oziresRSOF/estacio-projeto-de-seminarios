import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertProvider } from '../../../providers/alert/alert';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-category-edit',
  templateUrl: 'category-edit.html',
})
export class CategoryEditPage {
  categories = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private alertProvider: AlertProvider
  ) { }

  ionViewDidLoad() {
    this.setCategories();
  }

  setCategories() {
    this.storage.get('categories')
    .then(categories => this.categories = categories ? categories : [])
    .catch(err => this.alertProvider.showAlert("Erro ao buscar as categorias!", err));
  }

  removeCategory(id: number) {
    this.storage.get('categories')
    .then(categories => {
      categories = categories.filter(category => category.id != id);

      this.storage.set('categories', categories)
      .then(() => {
        this.categories = categories;
        this.alertProvider.showAlert("Categoria removida!", "Categoria removida com sucesso.");
      })
    })
    .catch(err => this.alertProvider.showAlert("Erro ao remover a categoria!", err));
  }

}

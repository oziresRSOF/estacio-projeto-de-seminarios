import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertProvider } from '../../providers/alert/alert';
import { User } from '../../shared/interfaces/user.interface';
import { UserService } from '../../shared/services/user.service';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-swap-password',
  templateUrl: 'swap-password.html',
})
export class SwapPasswordPage {
  formGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    oldPassword: new FormControl('', [Validators.required]),
    newPassword: new FormControl('', [Validators.required]),
    currentPassword: new FormControl('', [Validators.required])
  });

  user: User;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertProvider: AlertProvider,
    private userService: UserService
  ) { }

  ionViewDidLoad() {
    this.user = this.userService.getCurrentUser();
    this.formGroup.patchValue({ name: this.user.name, email: this.user.email, currentPassword: this.user.password });
  }

  submit() {
    if(this.formGroup.valid && this.formGroup.value.oldPassword === this.formGroup.value.currentPassword) {
      this.userService.updatePassword(this.formGroup.value.newPassword);
      this.alertProvider.showAlert('Senha alterada!', 'Senha alterada com sucesso.', () => this.goToHome());
    }
    else {
      this.alertProvider.showAlert('Senha antiga inválida!', 'Verifique se a senha antiga é igual à atual.');
    }
  }

  goToHome() {
    this.navCtrl.push(HomePage);
  }
}

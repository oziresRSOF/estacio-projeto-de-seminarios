import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { SwapPasswordPage } from './swap-password';

@NgModule({
  declarations: [
    SwapPasswordPage,
  ],
  imports: [
    IonicPageModule.forChild(SwapPasswordPage),
  ]
})
export class SwapPasswordPageModule {}

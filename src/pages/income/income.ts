import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import * as _ from 'underscore';
import { Income } from '../../shared/interfaces/income.interface';
import { IncomeService } from '../../shared/services/income.service';
import { IncomeEditPage } from './income-edit/income-edit';

interface IFilter {
  value: string; 
  label: string;
}

@Component({
  selector: 'page-income',
  templateUrl: 'income.html',
})
export class IncomePage {
  incomeList: Income[];
  filterList: IFilter[];
  selectedFilter: string;
  constructor(public navCtrl: NavController, 
              private incomeService: IncomeService
  ) { 
    this.filterList = [
      { value: 'ALL',           label: 'Todos' },
      { value: 'LAST_1_MONTH',  label: 'Último mês' }
    ]
  }

  ionViewDidEnter() {
    this.incomeList = this.incomeService.getIncomes();
    this.configureIncomeList();
  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.incomeList = this.incomeService.getIncomes();
      this.configureIncomeList();
      this.selectedFilter = this.filterList[0].value;
    }, 100);
  }

  private configureIncomeList() {
    this.incomeList = _.sortBy(this.incomeList, 'date').reverse();
  }

  editIncome(income: Income): void {
    this.navCtrl.push(IncomeEditPage, income);
  }

  selectFilter(filter: IFilter): void {
    this.incomeList = this.incomeService.getIncomesFilteredByPeriod(filter.value);
    this.configureIncomeList();
  }

}

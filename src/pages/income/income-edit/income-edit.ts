import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController, NavController, NavParams } from 'ionic-angular';
import * as _ from 'underscore';
import { AlertProvider } from './../../../providers/alert/alert';
import { Category } from './../../../shared/interfaces/category.interface';
import { Income } from './../../../shared/interfaces/income.interface';
import { CategoryService } from './../../../shared/services/category.service';
import { IncomeService } from './../../../shared/services/income.service';
import { CategoryAddPage } from './../../category/category-add/category-add';

@Component({
    selector: 'page-income-edit',
    templateUrl: 'income-edit.html',
})
export class IncomeEditPage {
    formGroup = new FormGroup({
        categoryId: new FormControl('', [Validators.required]),
        name: new FormControl('', [Validators.required]),
        value: new FormControl('', [Validators.required]),
        date: new FormControl('', [Validators.required])
    });

    categoryList: Category[];

    constructor(
        public navCtrl: NavController,
        private navParams: NavParams,
        private categoryService: CategoryService,
        private incomeService: IncomeService,
        private modalCtrl: ModalController,
        private alertProvider: AlertProvider
    ) { }

    ionViewDidLoad() {
        this.formGroup.patchValue(this.navParams.data);

        this.categoryList = [];
        setTimeout(() => {
            this.configureCategoryList();
        }, 100);
    }

    private configureCategoryList() {
        this.categoryList = _.sortBy(this.categoryService.getCategories(), 'name');
        this.categoryList.unshift(<Category>{ id: 0, name: 'Cadastrar categoria' });
    }

    selectCategory(category: Category) {
        if (category.id === 0) {
            const modal = this.modalCtrl.create(CategoryAddPage);
            modal.onDidDismiss(data => {
                if (data) {
                    this.categoryList.shift()
                    this.categoryList.push(data);
                    this.configureCategoryList();
                    this.formGroup.get('categoryId').setValue(data.id);
                }
                else {
                    this.formGroup.get('categoryId').reset();
                }
            });
            modal.present();
        }
    }

    submit() {
        if (this.formGroup.valid) {
            let income: Income = this.formGroup.value;
            income.id = this.navParams.data.id;
            income.userId = this.navParams.data.userId;
            this.incomeService.updateIncome(income);
            this.alertProvider.showAlert('', 'Receita atualizada com sucesso!', () => this.navCtrl.pop());
        }
    }

  removeIncome(): void {
      this.incomeService.removeIncomeById(this.navParams.data.id);
      this.alertProvider.showAlert('', 'Receita apagada com sucesso!', () => this.navCtrl.pop());
  }
}

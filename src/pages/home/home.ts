import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import * as _ from 'underscore';
import { Charge } from '../../shared/interfaces/charge.interface';
import { User } from '../../shared/interfaces/user.interface';
import { ChargeService } from '../../shared/services/charge.service';
import { UserService } from '../../shared/services/user.service';
import { ChargeAddPage } from '../charge/charge-add/charge-add';
import { ChargeEditPage } from '../charge/charge-edit/charge-edit';
import { IncomePage } from '../income/income';
import { Income } from './../../shared/interfaces/income.interface';
import { IncomeService } from './../../shared/services/income.service';
import { ChargePage } from './../charge/charge';
import { IncomeAddPage } from './../income/income-add/income-add';
import { IncomeEditPage } from './../income/income-edit/income-edit';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  user: User;
  incomeList: Income[];
  chargeList: Charge[];
  limitToShow: number;

  constructor(public navCtrl: NavController, 
              private incomeService: IncomeService,
              private chargeService: ChargeService,
              private userService: UserService
  ) { 
    this.limitToShow = 3;
  }
  
  ionViewDidEnter() {
    this.incomeList = this.incomeService.getIncomes();
    this.configureIncomeList();

    this.chargeList = this.chargeService.getCharges();
    this.configureChargeList();

    this.configureBalance();
  }
  
  ionViewDidLoad() {
    this.user = this.userService.getCurrentUser();
    setTimeout(() => {
      this.incomeList = this.incomeService.getIncomes();
      this.configureIncomeList();

      this.chargeList = this.chargeService.getCharges();
      this.configureChargeList();

      this.configureBalance();
    }, 100);
  }

  private configureIncomeList(): void {
    this.incomeList = _.sortBy(this.incomeList, 'date').reverse();
  }

  private configureChargeList(): void {
    this.chargeList = _.sortBy(this.chargeList, 'date').reverse();
  }

  private configureBalance(): void {
    this.user.balance = this.incomeService.getTotal() - this.chargeService.getTotal();
  }

  addIncome(): void {
    this.navCtrl.push(IncomeAddPage);
  }

  goToIncomePage(): void {
    this.navCtrl.push(IncomePage);
  }
  
  editIncome(income: Income): void {
    this.navCtrl.push(IncomeEditPage, income);
  }

  addCharge(): void {
    this.navCtrl.push(ChargeAddPage);
  }

  goToChargePage(): void {
    this.navCtrl.push(ChargePage);
  }
  
  editCharge(charge: Charge): void {
    this.navCtrl.push(ChargeEditPage, charge);
  }

}

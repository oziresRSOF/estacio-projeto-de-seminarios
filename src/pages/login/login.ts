import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertController, IonicPage, LoadingController, MenuController, NavController, NavParams } from 'ionic-angular';
import { User } from '../../shared/interfaces/user.interface';
import { UserService } from '../../shared/services/user.service';
import { HomePage } from '../home/home';
import { RegisterPage } from '../register/register';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {
  formGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required])
  });

  constructor(
    private menu: MenuController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private userService: UserService,
    private loadingCtrl: LoadingController) { 
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }

  ionViewWillLeave() {
    this.menu.swipeEnable(true);
   }


  ionViewDidLoad() {
    if (this.navCtrl.last() !== this.navCtrl.getActive()) {
      this.navCtrl.remove(this.navCtrl.last().index);
    }

    this.userService.getLastActiveUser().then((user) => {
      if(user) {
        this.showLoading();
        setTimeout(() => {
          this.authenticateAndGoToHomePage(user);
        }, 1000);
      }
    });
  }

  login() {
    let dbUser:User = this.userService.getUserByEmailAndPassword(this.formGroup.value.email, 
                                                                 this.formGroup.value.password);

    if(dbUser) {
      this.authenticateAndGoToHomePage(dbUser);
    }
    else {
      this.showAlert('Falha no login', 'E-mail ou senha inválido');
    }
  }

  private authenticateAndGoToHomePage(user: User) {
    this.userService.setCurrentUser(user);
    this.navCtrl.setRoot(HomePage);
  }

  goToRegisterPage() {
    this.navCtrl.push(RegisterPage);
  }

  private showAlert(title: string, message: string) {
    const alert = this.alertCtrl.create({ title: title, subTitle: message, buttons: ['OK'] });
    alert.present();
  }

  private showLoading() {
    const loader = this.loadingCtrl.create({
      content: "Aguarde...",
      dismissOnPageChange: true
    });
    loader.present();
  }
}

import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserService } from '../../shared/services/user.service';
import { HomePage } from '../home/home';


@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  formGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required])
  });

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private userService: UserService
  ) { }

  ionViewDidLoad() { }

  submit() {
    if(this.formGroup.valid) {
      this.userService.registerNewUser(this.formGroup.value);
      this.navCtrl.setRoot(HomePage);
    }
  }
}

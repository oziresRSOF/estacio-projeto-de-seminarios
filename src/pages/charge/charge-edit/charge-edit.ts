import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController, NavController, NavParams } from 'ionic-angular';
import * as _ from 'underscore';
import { AlertProvider } from './../../../providers/alert/alert';
import { Category } from './../../../shared/interfaces/category.interface';
import { Charge } from './../../../shared/interfaces/charge.interface';
import { CategoryService } from './../../../shared/services/category.service';
import { ChargeService } from './../../../shared/services/charge.service';
import { CategoryAddPage } from './../../category/category-add/category-add';

@Component({
    selector: 'page-charge-edit',
    templateUrl: 'charge-edit.html',
})
export class ChargeEditPage {
    formGroup = new FormGroup({
        categoryId: new FormControl('', [Validators.required]),
        name: new FormControl('', [Validators.required]),
        value: new FormControl('', [Validators.required]),
        date: new FormControl('', [Validators.required])
    });

    categoryList: Category[];

    constructor(
        public navCtrl: NavController,
        private navParams: NavParams,
        private categoryService: CategoryService,
        private chargeService: ChargeService,
        private modalCtrl: ModalController,
        private alertProvider: AlertProvider
    ) { }

    ionViewDidLoad() {
        this.formGroup.patchValue(this.navParams.data);

        this.categoryList = [];
        setTimeout(() => {
            this.configureCategoryList();
        }, 100);
    }

    private configureCategoryList() {
        this.categoryList = _.sortBy(this.categoryService.getCategories(), 'name');
        this.categoryList.unshift(<Category>{ id: 0, name: 'Cadastrar categoria' });
    }

    selectCategory(category: Category) {
        if (category.id === 0) {
            const modal = this.modalCtrl.create(CategoryAddPage);
            modal.onDidDismiss(data => {
                if (data) {
                    this.categoryList.shift()
                    this.categoryList.push(data);
                    this.configureCategoryList();
                    this.formGroup.get('categoryId').setValue(data.id);
                }
                else {
                    this.formGroup.get('categoryId').reset();
                }
            });
            modal.present();
        }
    }

    submit() {
        if (this.formGroup.valid) {
            let charge: Charge = this.formGroup.value;
            charge.id = this.navParams.data.id;
            charge.userId = this.navParams.data.userId;
            this.chargeService.updateCharge(charge);
            this.alertProvider.showAlert('', 'Gasto atualizada com sucesso!', () => this.navCtrl.pop());
        }
    }

  removeCharge(): void {
      this.chargeService.removeChargeById(this.navParams.data.id);
      this.alertProvider.showAlert('', 'Gasto apagada com sucesso!', () => this.navCtrl.pop());
  }
}

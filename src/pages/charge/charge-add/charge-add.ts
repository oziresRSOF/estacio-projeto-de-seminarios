import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController, NavController } from 'ionic-angular';
import * as _ from 'underscore';
import { AlertProvider } from './../../../providers/alert/alert';
import { Category } from './../../../shared/interfaces/category.interface';
import { CategoryService } from './../../../shared/services/category.service';
import { ChargeService } from './../../../shared/services/charge.service';
import { CategoryAddPage } from './../../category/category-add/category-add';

@Component({
  selector: 'page-charge-add',
  templateUrl: 'charge-add.html',
})
export class ChargeAddPage {
  formGroup = new FormGroup({
    categoryId: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required]),
    value: new FormControl('', [Validators.required]),
    date: new FormControl('', [Validators.required])
  });

  categoryList: Category[];

  constructor(
    public navCtrl: NavController,
    private categoryService: CategoryService,
    private chargeService: ChargeService,
    private modalCtrl: ModalController,
    private alertProvider: AlertProvider
  ) { }

  ionViewDidLoad() {
    this.categoryList = [];
    setTimeout(() => {
      this.configureCategoryList();
    }, 100);
  }

  private configureCategoryList() {
    this.categoryList = _.sortBy(this.categoryService.getCategories(), 'name');
    this.categoryList.unshift(<Category>{ id: 0, name: 'Cadastrar categoria' });
  }

  selectCategory(category: Category) {
    if (category.id === 0) {
      const modal = this.modalCtrl.create(CategoryAddPage);
      modal.onDidDismiss(data => {
        if (data) {
          this.categoryList.shift()
          this.categoryList.push(data);
          this.configureCategoryList();
          this.formGroup.get('categoryId').setValue(data.id);
        }
        else {
          this.formGroup.get('categoryId').reset();
        }
      });
      modal.present();
    }
  }

  submit() {
    if (this.formGroup.valid) {
      this.chargeService.registerNewCharge(this.formGroup.value);
      this.alertProvider.showAlert('', 'Gasto cadastrada com sucesso!', () => this.navCtrl.pop());
    }
  }
}

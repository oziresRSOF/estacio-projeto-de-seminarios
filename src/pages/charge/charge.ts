import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import * as _ from 'underscore';
import { Charge } from '../../shared/interfaces/charge.interface';
import { ChargeService } from '../../shared/services/charge.service';
import { ChargeEditPage } from './charge-edit/charge-edit';

interface IFilter {
  value: string; 
  label: string;
}

@Component({
  selector: 'page-charge',
  templateUrl: 'charge.html',
})
export class ChargePage {
  chargeList: Charge[];
  filterList: IFilter[];
  selectedFilter: string;
  constructor(public navCtrl: NavController, 
              private chargeService: ChargeService
  ) { 
    this.filterList = [
      { value: 'ALL',           label: 'Todos' },
      { value: 'LAST_1_MONTH',  label: 'Último mês' }
    ]
  }

  ionViewDidEnter() {
    this.chargeList = this.chargeService.getCharges();
    this.configureChargeList();
  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.chargeList = this.chargeService.getCharges();
      this.configureChargeList();
      this.selectedFilter = this.filterList[0].value;
    }, 100);
  }

  private configureChargeList() {
    this.chargeList = _.sortBy(this.chargeList, 'date').reverse();
  }

  editCharge(charge: Charge): void {
    this.navCtrl.push(ChargeEditPage, charge);
  }

  selectFilter(filter: IFilter): void {
    this.chargeList = this.chargeService.getChargesFilteredByPeriod(filter.value);
    this.configureChargeList();
  }

}

import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';

@Injectable()
export class AlertProvider {

  constructor(private alertCtrl: AlertController) { }

  showAlert(title: string, message: string, callback?) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [
        {
          text: 'OK',
          role: 'ok',
          handler: () => {
            if(callback) {
              callback();
            }
          }
        }
      ]
    });
    
    alert.present();
  }

}

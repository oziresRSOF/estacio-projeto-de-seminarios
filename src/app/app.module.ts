import { registerLocaleData } from '@angular/common';
import ptBr from '@angular/common/locales/pt';
import { ErrorHandler, LOCALE_ID, NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { CategoryAddPage } from '../pages/category/category-add/category-add';
import { CategoryEditPage } from '../pages/category/category-edit/category-edit';
import { ChargeAddPage } from '../pages/charge/charge-add/charge-add';
import { ChargeEditPage } from '../pages/charge/charge-edit/charge-edit';
import { HomePage } from '../pages/home/home';
import { IncomePage } from '../pages/income/income';
import { IncomeAddPage } from '../pages/income/income-add/income-add';
import { IncomeEditPage } from '../pages/income/income-edit/income-edit';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { SwapPasswordPage } from '../pages/swap-password/swap-password';
import { AlertProvider } from '../providers/alert/alert';
import { CategoryService } from '../shared/services/category.service';
import { ChargeService } from '../shared/services/charge.service';
import { IncomeService } from '../shared/services/income.service';
import { UserService } from '../shared/services/user.service';
import { UtilService } from '../shared/services/util.service';
import { ChargePage } from './../pages/charge/charge';
import { MyApp } from './app.component';

registerLocaleData(ptBr, 'pt');

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomePage,
    RegisterPage,
    SwapPasswordPage,
    CategoryAddPage,
    CategoryEditPage,
    IncomePage,
    IncomeAddPage,
    IncomeEditPage,
    ChargePage,
    ChargeAddPage,
    ChargeEditPage
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    RegisterPage,
    SwapPasswordPage,
    CategoryAddPage,
    CategoryEditPage,
    IncomePage,
    IncomeAddPage,
    IncomeEditPage,
    ChargePage,
    ChargeAddPage,
    ChargeEditPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: LOCALE_ID, useValue: 'pt' },
    AlertProvider,
    UserService,
    CategoryService,
    IncomeService,
    ChargeService,
    UtilService,
  ]
})
export class AppModule {}

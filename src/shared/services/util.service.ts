import { Injectable } from "@angular/core";

@Injectable()
export class UtilService {
    formatDate(date: Date) {
        let day: string = date.getDate().toString();
        let month: string = (date.getMonth() + 1).toString();
        let year: string = date.getFullYear().toString();
        
        if(day.length < 2) {
            day = '0' + day;
        }

        if(month.length < 2) {
            month = '0' + month;
        }

        return `${year}-${month}-${day}`;
    }
}
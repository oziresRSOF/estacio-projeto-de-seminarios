import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Category } from './../interfaces/category.interface';
import { UserService } from './user.service';

@Injectable()
export class CategoryService {
    private categoryList: Category[];
    private userId: number;
    private userCategoriesKey: string;

    constructor(private userService: UserService, private storage: Storage) {
        this.categoryList = [];
        this.userId = this.userService.getCurrentUser().id;
        this.userCategoriesKey = this.userId + '_categoryList';
        this.storage.get(this.userCategoriesKey).then(categoryList => this.categoryList = categoryList || []);
    }

    getCategories(): Category[] {
        return this.categoryList;
    }

    registerNewCategory(category: Category): Category {
        category.id = new Date().getTime();
        category.userId = this.userId;
        this.updateCategory(category);
        return category;
    }

    private updateCategory(category: Category): void {
        let dbCategory: Category = this.categoryList.find(x => x.id === category.id);
        
        if(dbCategory) {
            this.categoryList.splice(this.categoryList.indexOf(dbCategory), 1);
        }

        this.categoryList.push(category);
        this.storage.set(this.userCategoriesKey, this.categoryList);
    }
}
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Income } from '../interfaces/income.interface';
import { UserService } from './user.service';
import { UtilService } from './util.service';

@Injectable()
export class IncomeService {
    private incomeList: Income[];
    private userId: number;
    private userIncomesKey: string;
    
    constructor(private userService: UserService, 
                private storage: Storage, 
                private utilService: UtilService
    ) {
        this.userId = this.userService.getCurrentUser().id;
        this.userIncomesKey = this.userId + '_incomeList';
        this.storage.get(this.userIncomesKey).then(incomeList => this.incomeList = incomeList || []);
    }

    getIncomes(): Income[] {
        return this.incomeList;
    }

    getIncomesFilteredByPeriod(period: string): Income[] {
        let today: string = this.utilService.formatDate(new Date());
        let thisMonth: string = today.substr(0, 7);

        if(period === 'LAST_1_MONTH') {
            return this.incomeList.filter(income => {
                return income.date.toString().substr(0, 7) === thisMonth;
            });
        }

        return this.incomeList;
    }

    registerNewIncome(income: Income): Income {
        income.id = new Date().getTime();
        income.userId = this.userId;
        this.updateIncome(income);
        return income;
    }

    updateIncome(income: Income): void {
        let dbIncome: Income = this.incomeList.find(x => x.id === income.id);
        
        if(dbIncome) {
            this.incomeList.splice(this.incomeList.indexOf(dbIncome), 1);
        }

        this.incomeList.push(income);
        this.storage.set(this.userIncomesKey, this.incomeList);
    }

    removeIncomeById(id: number): void {
        let dbIncome: Income = this.incomeList.find(x => x.id === id);
        
        if(dbIncome) {
            this.incomeList.splice(this.incomeList.indexOf(dbIncome), 1);
        }

        this.storage.set(this.userIncomesKey, this.incomeList);
    }

    getTotal(): number {
        if(!this.incomeList || this.incomeList.length === 0) {
            return 0;
        }

        return this.incomeList.reduce((sum: number, income: Income) => sum + Number(income.value), 0);
    }
}
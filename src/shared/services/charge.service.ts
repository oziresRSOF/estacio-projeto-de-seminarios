import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Charge } from '../interfaces/charge.interface';
import { UserService } from './user.service';
import { UtilService } from './util.service';

@Injectable()
export class ChargeService {
    private chargeList: Charge[];
    private userId: number;
    private userChargesKey: string;
    
    constructor(private userService: UserService, 
                private storage: Storage, 
                private utilService: UtilService
    ) {
        this.userId = this.userService.getCurrentUser().id;
        this.userChargesKey = this.userId + '_chargeList';
        this.storage.get(this.userChargesKey).then(chargeList => this.chargeList = chargeList || []);
    }

    getCharges(): Charge[] {
        return this.chargeList;
    }

    getChargesFilteredByPeriod(period: string): Charge[] {
        let today: string = this.utilService.formatDate(new Date());
        let thisMonth: string = today.substr(0, 7);

        if(period === 'LAST_1_MONTH') {
            return this.chargeList.filter(charge => {
                return charge.date.toString().substr(0, 7) === thisMonth;
            });
        }

        return this.chargeList;
    }

    registerNewCharge(charge: Charge): Charge {
        charge.id = new Date().getTime();
        charge.userId = this.userId;
        this.updateCharge(charge);
        return charge;
    }

    updateCharge(charge: Charge): void {
        let dbCharge: Charge = this.chargeList.find(x => x.id === charge.id);
        
        if(dbCharge) {
            this.chargeList.splice(this.chargeList.indexOf(dbCharge), 1);
        }

        this.chargeList.push(charge);
        this.storage.set(this.userChargesKey, this.chargeList);
    }

    removeChargeById(id: number): void {
        let dbCharge: Charge = this.chargeList.find(x => x.id === id);
        
        if(dbCharge) {
            this.chargeList.splice(this.chargeList.indexOf(dbCharge), 1);
        }

        this.storage.set(this.userChargesKey, this.chargeList);
    }

    getTotal(): number {
        if(!this.chargeList || this.chargeList.length === 0) {
            return 0;
        }

        return this.chargeList.reduce((sum: number, charge: Charge) => sum + Number(charge.value), 0);
    }
}
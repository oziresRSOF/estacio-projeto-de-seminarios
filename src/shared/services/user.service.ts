import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { User } from "../interfaces/user.interface";

@Injectable()
export class UserService {
    private currentUser: User;
    private userList: User[];
    
    constructor(private storage: Storage) {
        this.storage.get('userList').then(userList => this.userList = userList || []);
    }

    setCurrentUser(user: User): void  {
        this.currentUser = user;
        this.storage.set('lastActiveUser', this.currentUser);
    }

    getCurrentUser(): User {
        return this.currentUser;
    }

    getLastActiveUser(): Promise<User> {
        return this.storage.get('lastActiveUser');
    }

    registerNewUser(user: User): void {
        user.id = new Date().getTime();
        this.updateUser(user);
        this.setCurrentUser(user);
    }

    getUserByEmailAndPassword(email: string, password: string): User {
        return this.userList.find(x => x.email === email && x.password === password);
    }

    updatePassword(newPassword: string) {
        this.currentUser.password = newPassword;
        this.updateUser(this.currentUser);
    }

    clearCurrentSession(): void {
        this.currentUser = null;
        this.storage.remove('lastActiveUser');
    }

    private updateUser(user: User): void {
        let dbuser: User = this.userList.find(x => x.id === user.id);
        
        if(dbuser) {
            this.userList.splice(this.userList.indexOf(dbuser), 1);
        }

        this.userList.push(user);
        this.storage.set('userList', this.userList);
    }
}
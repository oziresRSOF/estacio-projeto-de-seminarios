export interface Income {
    id: number;
    name: string;
    date: Date;
    value: number;
    userId: number;
    categoryId: number;
} 